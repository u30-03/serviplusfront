import React from 'react'
import './css/volt.css'
import { AppRouter } from './router/AppRouter'


export const App = () => {
  return (
    <>
      {/* <div>Hola</div> */}
      <AppRouter />
    </>
  )
}
