import React from "react";
import { TableHeadItem } from "./TableHeadItem";
import { TableRow } from "./TableRow";


export const Table = ({ theadData, tbodyData, customClass, customHeadClass }) => {
    return (
        <div className="card border-0 shadow mb-4">
            <div className="card-body">
                <div className="table-responsive">
                    {/* <table className="table table-centered table-nowrap mb-0 rounded"></table> */}
                    <table className={customClass}>
                        <thead className={customHeadClass}>
                            <tr>
                                {theadData.map((h) => {
                                    return <TableHeadItem key={h} item={h} />;
                                })}
                            </tr>
                        </thead>
                        <tbody>
                            {tbodyData.map((item) => {
                                return <TableRow key={item.id} data={item.items} />;
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
};