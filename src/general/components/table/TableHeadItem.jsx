import React from "react";

export const TableHeadItem = ({ item, customClass }) => {
    return (
        <th title={item} className={customClass}>
            {item}
        </th>
    );
};