import React from 'react'
import { MutatingDots } from 'react-loader-spinner'

export const LoadingPage = ({ message = "Cargando" }) => {
    return (
        <>
            <div className="h-100 d-flex justify-content-center align-items-center">
                <div className="h-100 d-flex justify-content-center align-items-center">
                    <h1>{message}</h1>
                </div>
                <div className="h-100 d-flex justify-content-center align-items-center">
                    <MutatingDots
                        height="100"
                        width="100"
                        color="rgba(var(--bs-gray-800-rgb), var(--bs-bg-opacity))"
                        secondaryColor='rgba(var(--bs-gray-800-rgb), var(--bs-bg-opacity))'
                        radius='12.5'
                        ariaLabel="mutating-dots-loading"
                        wrapperStyle={{}}
                        wrapperClass=""
                        visible={true}
                    />
                </div>
            </div>
        </>
    )
}

