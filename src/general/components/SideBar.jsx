import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';

import env from 'react-dotenv';
import axios from 'axios';
import { useJwt } from 'react-jwt';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faHome, faUsers, faCircleNodes, faClipboardUser, faUsersViewfinder, faTicket, faRightFromBracket } from '@fortawesome/free-solid-svg-icons'
/* import { } from '@fortawesome/free-regular-svg-icons' */

import { logOut } from '../../helpers/logout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

library.add(faHome, faUsers, faCircleNodes, faClipboardUser, faUsersViewfinder, faTicket, faRightFromBracket);

export const SideBar = () => {

    // obtener token
    const the_token = localStorage.getItem('the_token') || '';
    const resp = useJwt(the_token);

    const [uid, setUid] = useState('');
    const [rol, setRol] = useState('agente');


    useEffect(() => {
        if (resp.decodedToken && uid == '') {
            setUid(resp.decodedToken.uid)
            console.log(uid)
        }
    }, [resp])

    // chequear el rol
    useEffect(() => {

        if (uid !== '') {
            //let authUser;
            const getUser = async (idUser) => {
                let fullURI;
                let dbUser;

                try {
                    fullURI = `${env.BACK_URI}/admins/${idUser}`;
                    dbUser = await axios.get(
                        fullURI,
                        {
                            headers: { "auth-token": the_token }
                        }
                    );

                    if (dbUser.status = 200) {
                        return dbUser;
                    }
                } catch (error) {
                    fullURI = `${env.BACK_URI}/agentes/${idUser}`;
                    dbUser = await axios.get(
                        fullURI,
                        {
                            headers: { "auth-token": the_token }
                        }
                    );

                    if (dbUser.status = 200) {
                        return dbUser;
                    }
                }
                return
            }

            getUser(uid).then((res) => {
                if (res.data.admin) {
                    //authUser = res.data.admin;
                    setRol('admin');
                }
                if (res.data.agente) {
                    //authUser = res.data.agente;
                }
            });
        }
    }, [uid])

    return (
        <>
            {/* <i className="fa-solid fa-screen-users"></i> */}

            <nav id="sidebarMenu" className="sidebar d-lg-block bg-gray-800 text-white collapse" data-simplebar>
                <div className="sidebar-inner px-4 pt-3">
                    <ul className="nav flex-column pt-3 pt-md-0">
                        <li className="nav-item ">
                            <a href="../main" className="nav-link">
                                {/* <i className="fa fa-solid fa-screen-users"></i> */}
                                <FontAwesomeIcon icon={faHome} />
                                &nbsp;&nbsp;&nbsp;
                                <span className="sidebar-text">Inicio</span>
                            </a>
                        </li>

                        {(rol == 'admin') ?
                            (<>
                                <li className="nav-item">
                                    <a href="../areas" className="nav-link">
                                        {/* <span className="sidebar-icon">
                                            <i className="fa-solid fa-screen-users"></i>
                                        </span> */}
                                        <FontAwesomeIcon icon={faCircleNodes} />
                                        &nbsp;&nbsp;&nbsp;
                                        <span className="sidebar-text">Áreas</span>
                                    </a>
                                </li>

                                <li className="nav-item ">
                                    <a href="../agentes" className="nav-link">
                                        {/* <span className="sidebar-icon">
                                            <i className="fa-solid fa-screen-users"></i>
                                        </span> */}
                                        <FontAwesomeIcon icon={faUsers} />
                                        &nbsp;&nbsp;&nbsp;
                                        <span className="sidebar-text">Agentes</span>
                                    </a>
                                </li>

                                <li className="nav-item ">
                                    <a href="../admins" className="nav-link">
                                        {/* <span className="sidebar-icon">
                                            <i className="fa-solid fa-screen-users"></i>
                                        </span> */}
                                        <FontAwesomeIcon icon={faClipboardUser} />
                                        &nbsp;&nbsp;&nbsp;
                                        <span className="sidebar-text">Administradores</span>
                                    </a>
                                </li>
                            </>) :
                            (<></>)
                        }

                        <li className="nav-item ">
                            <a href="../clientes" className="nav-link">
                                {/* <span className="sidebar-icon">
                                    <i className="fa-solid fa-screen-users"></i>
                                </span> */}
                                <FontAwesomeIcon icon={faUsersViewfinder} />
                                &nbsp;&nbsp;&nbsp;
                                <span className="sidebar-text">Clientes</span>
                            </a>
                        </li>

                        <li className="nav-item ">
                            <a href="../tickets" className="nav-link">
                                {/* <span className="sidebar-icon">
                                    <i className="fa-solid fa-screen-users"></i>
                                </span> */}
                                <FontAwesomeIcon icon={faTicket} />
                                &nbsp;&nbsp;&nbsp;
                                <span className="sidebar-text">Tickets</span>
                            </a>
                        </li>

                        <li className="nav-item ">
                            <a href="../home" onClick={logOut} className="nav-link">
                                {/* <span className="sidebar-icon">
                                    <i className="fa-solid fa-screen-users"></i>
                                </span> */}
                                <FontAwesomeIcon icon={faRightFromBracket} />
                                &nbsp;&nbsp;&nbsp;
                                <span className="sidebar-text">Cerrar sesión</span>
                            </a>
                        </li>

                        {/* <li className="nav-item">
                        <span className="nav-link d-flex justify-content-between align-items-center collapsed" data-bs-toggle="collapse" data-bs-target="#submenu-pages" aria-expanded="false">
                            <span>
                                <span className="sidebar-icon">
                                    <svg className="icon icon-xs me-2" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M2 5a2 2 0 012-2h8a2 2 0 012 2v10a2 2 0 002 2H4a2 2 0 01-2-2V5zm3 1h6v4H5V6zm6 6H5v2h6v-2z" clipRule="evenodd"></path><path d="M15 7h1a2 2 0 012 2v5.5a1.5 1.5 0 01-3 0V7z"></path></svg>
                                </span>
                                <span className="sidebar-text">Page examples</span>
                            </span>
                            <span className="link-arrow">
                                <svg className="icon icon-sm" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clipRule="evenodd"></path></svg>
                            </span>
                        </span>
                        <div className="multi-level collapse" role="list" id="submenu-pages" aria-expanded="false">
                            <ul className="flex-column nav">
                                <li className="nav-item">
                                    <a className="nav-link" href="../../pages/examples/sign-in.html">
                                        <span className="sidebar-text">Sign In</span>
                                    </a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="../../pages/examples/sign-up.html">
                                        <span className="sidebar-text">Sign Up</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li> */}
                    </ul>
                </div>
            </nav>
        </>
    )
}
