import { Navigate, Route, Routes } from 'react-router-dom';
import { Area, AreasList, Main, Agente, AgenteList, AdminList, Admin, ClienteList, Cliente, TicketList, Ticket } from '../../pages';
/* import { Navbar } from '../../ui'; */

export const ServiPlusRoutes = () => {
    return (
        <>
            {/* <Navbar /> */}

            {/* <div className="container"> */}
            <Routes>
                <Route path="/areas" element={<AreasList />} />
                <Route path="/area" element={<Area />} />

                <Route path="/agentes" element={<AgenteList />} />
                <Route path="/agente" element={<Agente />} />

                <Route path="/admins" element={<AdminList />} />
                <Route path="/admin" element={<Admin />} />

                <Route path="/clientes" element={<ClienteList />} />
                <Route path="/cliente" element={<Cliente />} />

                <Route path="/tickets" element={<TicketList />} />
                <Route path="/ticket" element={<Ticket />} />

                <Route path="main" element={<Main />} />

                <Route path="/*" element={<Navigate to="/main" />} />

            </Routes>
            {/* </div> */}


        </>
    )
}