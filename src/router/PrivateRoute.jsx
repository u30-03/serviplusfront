/* import { useContext } from 'react'; */
import axios from 'axios';
import env from "react-dotenv";
import { useEffect, useState } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { LoadingPage } from '../general/components';

/* import { AuthContext } from '../auth'; */

export const PrivateRoute = ({ children }) => {

    /* const { logged } = useContext( AuthContext ); */
    /* const { pathname, search } = useLocation(); */

    /* const lastPath = pathname + search;
    localStorage.setItem('lastPath', lastPath );     */

    // state to store whether the auth check has completed.
    const [loading, setLoading] = useState(true);

    // state to store whether the user is authenticated
    const [loggedIn, setLoggedIn] = useState(false);

    // state to store any error encounted while running the auth check.
    const [error, setError] = useState();

    const navigate = useNavigate();

    const the_token = localStorage.getItem('the_token') || '';

    useEffect(() => {
        // create async function to avoid the use of promise .then spam
        // this is just a preference coming from C# background prefer the 
        // async / await syntax.
        const checkLogin = async () => {
            try {
                // run the auth check.
                const fullURI = `${env.BACK_URI}/auth/check`
                const response = await axios.get(fullURI, {
                    headers: { "auth-token": the_token }
                });

                // set whether the user is logged in.
                setLoggedIn(!!response?.data?.loggedIn)
                //setLoggedIn(!!(response?.status === 200))
                // console.log("logged", response)

                // set that no error was encountered.
                setError(undefined);

                // start redirection if not logged in.
                if (!response?.data?.loggedIn) {
                    //if (!(response?.status === 200)) {
                    navigate("/home", { replace: true });
                }
            }
            // catch any error with the request.
            catch (err) {
                // set user to not logged in.
                setLoggedIn(false);

                // set the error to the caught exception.
                setError(err);

                // add another redirect call if you don't want to display an error when the request fails
                // e.g.: navigate("/" , {replace: true});
            }

            // request is finished set loading to false.
            setLoading(false);
        };

        checkLogin();
    }, [setLoading, setError, setLoggedIn])

    // This is the view while the auth check is being carried out:
    if (loading) {
        return (
            <>
                <LoadingPage />
            </>
        );
    }

    // This is the view if the auth check encounters an error:
    if (error) {
        return (
            <pre>
                {JSON.stringify(error, null, '\t')}
            </pre>
        );
    }

    // This is the view if the user was not authenticated
    // As the redirect is started as part of the useEffect this will
    // most likely not be displayed for any significant (or even preceivable)
    // amount of time.
    if (!loggedIn) {
        return (
            <>
                I am redirecting...
                <Navigate to="/home" />
            </>
        );
    }

    // This is the view if the user was authenticated.
    // Example view below renders the child elements that have been provided i.e. <Home /> in your snippet.
    return (
        <>
            {children}
        </>
    )



    /* let logged = !!localStorage.getItem('the_token');

    return (logged)
        ? children
        : <Navigate to="/home" /> */
}