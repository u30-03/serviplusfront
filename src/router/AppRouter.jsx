import { Navigate, Route, Routes } from 'react-router-dom';

/* import { HeroesRoutes } from '../heroes';
import { LoginPage } from '../auth'; */

import { ServiPlusRoutes } from '../general/index.js';
import { LoginPage } from '../auth';
import { MainPage } from '../ui';
import { PrivateRoute } from './PrivateRoute.jsx';
import { PublicRoute } from './PublicRoute.jsx';



export const AppRouter = () => {
    return (
        <>

            <Routes>
                <Route path="/home" element={
                    <PublicRoute>
                        <Routes>
                            <Route path="/" element={<MainPage />} />
                            <Route path="/*" element={<Navigate to="/" />} />
                        </Routes>
                    </PublicRoute>
                }
                />

                <Route path="login/*" element={
                    <PublicRoute>
                        {/* <LoginPage /> */}
                        <Routes>
                            <Route path="/*" element={<LoginPage />} />
                        </Routes>
                    </PublicRoute>
                }
                />


                <Route path="/*" element={
                    <PrivateRoute>
                        <ServiPlusRoutes />
                    </PrivateRoute>
                } />

                {/* <Route path="login" element={<LoginPage />} /> */}
                {/* <Route path="/*" element={ <HeroesRoutes />} /> */}

            </Routes>

        </>
    )
}