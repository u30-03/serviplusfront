import { toast } from 'react-toastify';

export const callToastWithNavigation = (
    message = "próxima página", 
    { 
      autoClose = 1000, 
      position = "top-center", 
      type = "success", 
      hideProgressBar = false,
      closeOnClick = true, 
      pauseOnHover = true, 
      draggable = true,
    },
    toastCallback,
  ) => {
    const navigationTimeout = autoClose + 500;
  
    toast(message, { autoClose, position, type, hideProgressBar, closeOnClick, pauseOnHover, draggable });
    setTimeout(() => {
      toastCallback();
    }, navigationTimeout);
  };