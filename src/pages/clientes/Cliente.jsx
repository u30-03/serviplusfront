import React, { useState } from 'react'
import { useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

import env from "react-dotenv";
import axios from 'axios';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { useAxios, useForm } from '../../hooks';
import { SideBar } from '../../general/components';
import { callToastWithNavigation } from '../../helpers';

export const Cliente = () => {
    const location = useLocation();
    let tipo = location.state?.tipo || "crear";
    let id = location.state?.id || "";
    const navigate = useNavigate();

    // obtener token
    const the_token = localStorage.getItem('the_token') || '';

    // Inicializar formulario
    const {
        setFormState, formState, onInputChange, onResetForm,
        nombre, email, telefono
    } = useForm({
        nombre: '',
        email: '',
        telefono: ''
    });


    let theError;
    const onCancel = () => {
        navigate("/clientes");
    }

    // Cargar info si es para edición
    useEffect(() => {
        try {
            if (tipo == "editar") {
                const getCliente = async (idCliente) => {
                    const fullURI = `${env.BACK_URI}/clientes/${idCliente}`;
                    const resOld = await axios.get(
                        fullURI,
                        {
                            headers: { "auth-token": the_token }
                        }
                    );
                    return resOld;
                }

                getCliente(id).then((res) => {
                    if (res.data.cliente) {
                        const cliente = res.data.cliente;
                        //console.log(cliente)
                        setFormState(
                            {
                                ...formState,
                                nombre: cliente.nombreCompleto,
                                email: cliente.email,
                                telefono: cliente.telefono
                            }
                        )
                    }
                });
            }
        } catch (error) {
            toast.error(error.response);
        }
    }, [tipo])

    // Guardar
    const onSave = async (event) => {
        event.preventDefault();

        // Guardar nuevo registro
        if (tipo == "crear") {
            const fullURI = `${env.BACK_URI}/clientes`;
            try {
                //en el endpoint (back) se verifica si existe y el estado es = false, para actualizar en lugar de crear
                console.log("acá")
                const res = await axios.post(
                    fullURI,
                    {
                        nombreCompleto: nombre,
                        email,
                        telefono
                    },
                    {
                        headers: { "auth-token": the_token }
                    }
                )

                // confirmar registro
                if (res.status == 201 || res.status == 200) {
                    //console.log(res)
                    callToastWithNavigation("Cliente registrado correctamente", { type: "success" }, () => navigate("/clientes"));
                }

            } catch (error) {
                theError = error.response;
                if (theError?.data?.errors?.length || 0 > 0) {
                    const concatErrors = theError.data.errors.reduce((prev, curr, idx) => idx == 0 ? curr.msg : prev + "\r\n" + curr.msg, "")
                    return toast.error(concatErrors, { style: { width: "600px" }, position: toast.POSITION.TOP_CENTER })
                } else {
                    return toast.error(theError);
                }
            }
        } else {
            // si es tipo edición
            try {
                const fullURI = `${env.BACK_URI}/clientes/${id}`;

                const newCliente = {
                    nombreCompleto: nombre,
                    email,
                    telefono,
                }
                // actualizar registro
                const res = await axios.put(
                    fullURI,
                    newCliente,
                    {
                        headers: { "auth-token": the_token }
                    }
                );

                // confirmar cambio
                if (res.status == 200) {
                    callToastWithNavigation("Cliente actualizado correctamente", { type: "success" }, () => navigate("/clientes"));
                }

            } catch (error) {
                console.log(error)
                theError = error.response;
                if (theError?.data.errors?.length || 0 > 0) {
                    const concatErrors = theError.data.errors.reduce((prev, curr, idx) => idx == 0 ? curr.msg : prev + "\r\n" + curr.msg, "")
                    return toast.error(concatErrors, { style: { width: "600px" }, position: toast.POSITION.TOP_CENTER })
                } else {
                    if (theError.data.msg) {
                        return toast.error(theError.data.msg);
                    } else {
                        return toast.error(theError);
                    }
                }
            }
        }
    }

    return (
        <>
            <SideBar />
            <main className="content">
                <div className="row mt-3">
                    <div className="col-12 col-xl-8">
                        <div className="card card-body border-0 shadow mb-4">
                            <h2 className="h5 mb-4">{(tipo == "crear") ? "Registrar nuevo cliente" : "Editar cliente"}</h2>
                            <form>
                                <div className="row">
                                    <div className="col-md-6 mb-3">
                                        <div>
                                            <label htmlFor="nombre">Nombre</label>
                                            <input value={nombre} onChange={onInputChange} name="nombre" className="form-control" id="nombre" type="text" placeholder="Ingresar un nombre" required />
                                        </div>
                                    </div>
                                    <div className="col-md-6 mb-3">
                                        <div>
                                            <label htmlFor="email">Email</label>
                                            <input value={email} onChange={onInputChange} name="email" className="form-control" id="email" type="email" placeholder="ejemplo@serviplus.com" required />
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-6 mb-3">
                                        <div>
                                            <label htmlFor="telefono">Teléfono</label>
                                            <input value={telefono} onChange={onInputChange} name="telefono" className="form-control" id="telefono" type="text" placeholder="" required />
                                        </div>
                                    </div>

                                </div>

                                <div className="row mt-2">
                                    <div className="col-md-2">
                                        <button onClick={(e) => onSave(e)} className="btn btn-gray-800 mt-2 animate-up-2 w-100" required>Guardar</button>
                                    </div>
                                    <div className="col-md-2">
                                        <button onClick={onCancel} className="btn btn-gray-800 mt-2 animate-up-2 w-100">Cancelar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </main>
        </>
    )
}

