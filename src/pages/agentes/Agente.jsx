import React, { useState } from 'react'
import { useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

import env from "react-dotenv";
import axios from 'axios';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { useAxios, useForm } from '../../hooks';
import { SideBar } from '../../general/components';
import { callToastWithNavigation } from '../../helpers';

export const Agente = () => {
    const location = useLocation();
    let tipo = location.state?.tipo || "crear";
    let id = location.state?.id || "";
    const navigate = useNavigate();

    // obtener token
    const the_token = localStorage.getItem('the_token') || '';

    // Inicializar formulario
    const {
        setFormState, formState, onInputChange, onResetForm,
        nombre, email, telefono, password1, password2, area
    } = useForm({
        password1: '',
        password2: '',
        nombre: '',
        email: '',
        telefono: '',
        area: ''
    });

    // lista de opciones de áreas 
    const [selOptions, setSelOptions] = useState([]);
    const { data: optData, isLoading: isLoadingOpt, error: errorOpt } = useAxios({
        method: 'get',
        url: `${env.BACK_URI}/areas/`,
        headers: {
            'auth-token': the_token
        },
        params: {
            limit: 1000000
        }
    });

    useEffect(() => {
        if (!isLoadingOpt && !errorOpt) {
            if (optData.areas) {
                setSelOptions(
                    optData.areas.map((area) => {
                        return {
                            value: area.nombreArea,
                            label: area.nombreArea,
                            _id: area._id
                        }
                    })
                );
                // actualizar el área en el state, si es creación
                if (tipo == "crear") {
                    setFormState({ ...formState, "area": optData.areas[0].nombreArea });
                }
            }
        }
    }, [isLoadingOpt])


    let theError;
    const onCancel = () => {
        navigate("/agentes");
    }

    // Cargar info si es para edición
    useEffect(() => {
        try {
            if (tipo == "editar") {
                const getAgente = async (idAgente) => {
                    const fullURI = `${env.BACK_URI}/agentes/${idAgente}`;
                    const resOld = await axios.get(
                        fullURI,
                        {
                            headers: { "auth-token": the_token }
                        }
                    );
                    return resOld;
                }

                getAgente(id).then((res) => {
                    if (res.data.agente) {
                        const agente = res.data.agente;
                        //console.log(agente)
                        setFormState(
                            {
                                ...formState,
                                area: agente.area.nombreArea,
                                nombre: agente.nombreCompleto,
                                email: agente.email,
                                telefono: agente.telefono
                            }
                        )
                    }
                });
            }
        } catch (error) {
            toast.error(error.response);
        }
    }, [tipo, isLoadingOpt])

    // Guardar
    const onSave = async (event) => {
        event.preventDefault();

        // organizar la contraseña
        setFormState({
            ...formState,
            password1: password1.trim(),
            password2: password2.trim()
        });

        if (password1 !== '' && password2 !== '' && password1 !== password2) {
            return toast.error("Las contraseñas no coinciden");
        }

        // Guardar nuevo registro
        if (tipo == "crear") {
            const fullURI = `${env.BACK_URI}/agentes`;
            try {
                //en el endpoint (back) se verifica si existe y el estado es = false, para actualizar en lugar de crear                
                const res = await axios.post(
                    fullURI,
                    {
                        nombreCompleto: nombre,
                        email,
                        telefono,
                        password: password1,
                        area
                    },
                    {
                        headers: { "auth-token": the_token }
                    }
                )

                // confirmar registro
                if (res.status == 201 || res.status == 200) {
                    //console.log(res)
                    callToastWithNavigation("Agente registrado correctamente", { type: "success" }, () => navigate("/agentes"));
                }

            } catch (error) {
                theError = error.response;
                if (theError?.data?.errors?.length || 0 > 0) {
                    const concatErrors = theError.data.errors.reduce((prev, curr, idx) => idx == 0 ? curr.msg : prev + "\r\n" + curr.msg, "")
                    return toast.error(concatErrors, { style: { width: "600px" }, position: toast.POSITION.TOP_CENTER })
                } else {
                    return toast.error(theError);
                }
            }
        } else {
            // si es tipo edición
            try {
                const fullURI = `${env.BACK_URI}/agentes/${id}`;

                const newAgente = {
                    nombreCompleto: nombre,
                    email,
                    telefono,
                    area
                }
                if (password1 !== '') {
                    newAgente.password = password1;
                    newAgente.solicitarCambio = true;
                }
                // actualizar registro
                const res = await axios.put(
                    fullURI,
                    newAgente,
                    {
                        headers: { "auth-token": the_token }
                    }
                );

                // confirmar cambio
                if (res.status == 200) {
                    callToastWithNavigation("Agente actualizado correctamente", { type: "success" }, () => navigate("/agentes"));
                }

            } catch (error) {
                theError = error.response;
                if (theError?.data.errors?.length || 0 > 0) {
                    const concatErrors = theError.data.errors.reduce((prev, curr, idx) => idx == 0 ? curr.msg : prev + "\r\n" + curr.msg, "")
                    return toast.error(concatErrors, { style: { width: "600px" }, position: toast.POSITION.TOP_CENTER })
                } else {
                    if (theError.data.msg) {
                        return toast.error(theError.data.msg);
                    } else {
                        return toast.error(theError);
                    }
                }
            }
        }
    }

    return (
        <>
            <SideBar />
            <main className="content">
                <div className="row mt-3">
                    <div className="col-12 col-xl-8">
                        <div className="card card-body border-0 shadow mb-4">
                            <h2 className="h5 mb-4">{(tipo == "crear") ? "Registrar nuevo agente" : "Editar agente"}</h2>
                            <form>
                                <div className="row">
                                    <div className="col-md-6 mb-3">
                                        <div>
                                            <label htmlFor="nombre">Nombre</label>
                                            <input value={nombre} onChange={onInputChange} name="nombre" className="form-control" id="nombre" type="text" placeholder="Ingresar un nombre" required />
                                        </div>
                                    </div>
                                    <div className="col-md-6 mb-3">
                                        <div>
                                            <label htmlFor="email">Email</label>
                                            <input value={email} onChange={onInputChange} name="email" className="form-control" id="email" type="email" placeholder="ejemplo@serviplus.com" required />
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-6 mb-3">
                                        <div>
                                            <label htmlFor="telefono">Teléfono</label>
                                            <input value={telefono} onChange={onInputChange} name="telefono" className="form-control" id="telefono" type="text" placeholder="" required />
                                        </div>
                                    </div>
                                    <div className="col-md-6 mb-3">
                                        <div>
                                            <label htmlFor="area">Área</label>
                                            {/* <input value={area} onChange={onInputChange} name="area" className="form-control" id="area" type="text" placeholder="" required /> */}
                                            {/* <Select value={area} onChange={onInputChange} name="area" id="area" options={selOptions} defaultValue={selOptions[1]} /> */}
                                            <select name="area" value={area} onChange={onInputChange} className="form-select w-100 mb-0" id="area" required>
                                                {
                                                    selOptions.map((opt) => (
                                                        <option key={opt._id} value={opt.value}>{opt.label}</option>
                                                    ))
                                                }
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-6 mb-3">
                                        <div>
                                            <label htmlFor="password1">Password</label>
                                            <input value={password1} onChange={onInputChange} name="password1" className="form-control" id="password1" type="password" placeholder="Ingresar password" required />
                                        </div>
                                    </div>
                                    <div className="col-md-6 mb-3">
                                        <div>
                                            <label htmlFor="password2">Repetir password</label>
                                            <input value={password2} onChange={onInputChange} name="password2" className="form-control" id="password2" type="password" placeholder="Repetir password" required />
                                        </div>
                                    </div>
                                </div>

                                <div className="row mt-2">
                                    <div className="col-md-2">
                                        <button onClick={(e) => onSave(e)} className="btn btn-gray-800 mt-2 animate-up-2 w-100" required>Guardar</button>
                                    </div>
                                    <div className="col-md-2">
                                        <button onClick={onCancel} className="btn btn-gray-800 mt-2 animate-up-2 w-100">Cancelar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </main>
        </>
    )
}

