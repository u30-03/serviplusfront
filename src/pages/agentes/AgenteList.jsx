import React from 'react'
import { useState } from 'react'
import { useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';

import env from "react-dotenv";
import { useAxios } from '../../hooks';

import { SideBar, Table } from '../../general/components'
import axios from 'axios';
import { callToastWithNavigation } from '../../helpers';


export const AgenteList = () => {
    const navigate = useNavigate();
    const the_token = localStorage.getItem('the_token');
    /* const [updatedAgente, setUpdatedAgente] = useState(''); */
    const [deletedAgente, setDeletedAgente] = useState('')

    const { data, isLoading, error } = useAxios({
        method: 'get',
        url: `${env.BACK_URI}/agentes/`,
        headers: {
            'auth-token': the_token
        },
        params: {
            limit: 1000000
        }
    });
    const [newData, setNewData] = useState([])

    const onDeleteAgente = async (idAgente) => {
        try {
            const res = await axios.delete(
                `${env.BACK_URI}/agentes/${idAgente}`,
                {
                    headers: { "auth-token": the_token }
                }
            );
            //console.log(res)
            if (res.status == 202) {
                if (res.data?.agenteDB?.nombreCompleto) {
                    callToastWithNavigation(`Agente eliminado correctamente: ${res.data.agenteDB.nombreCompleto}`, { type: "success" }, () => { })
                    setDeletedAgente(res.data.agenteDB.nombreCompleto);
                    //console.log(updatedAgente);
                }
            }
        } catch (error) {
            callToastWithNavigation("Ocurrió un error", { type: "error" }, () => { })
        }
    }

    const onNewAgente = () => {
        navigate("/agente");
    }

    const [tBody, setTBody] = useState([]);

    const updateBody = (the_data) => {
        if (the_data) {
            const newTBody = the_data.map(
                (agente, i) => (
                    {
                        id: agente.uid,
                        items: [
                            agente.nombreCompleto,
                            agente.email,
                            agente.telefono,
                            agente.area.nombreArea,
                            <span>
                                <Link to="/agente" state={{ tipo: "editar", id: agente.uid }} className="fa-solid fa-pen-to-square"></Link>
                                &nbsp;&nbsp;&nbsp;
                                <Link onClick={(e) => onDeleteAgente(agente.uid)} className="fa-solid fa-trash text-danger"></Link>
                            </span>
                        ]
                    }
                )
            );
            setTBody(newTBody);
        }
    }

    useEffect(() => {
        //updateBody(data)
        setNewData(data?.agentes)
    }, [data])

    useEffect(() => {
        updateBody(newData)
    }, [newData]);

    useEffect(() => {
        setNewData(
            (newData) => {
                if (newData) {
                    return newData.filter(agente => agente.nombreCompleto !== deletedAgente)
                }
            }
        )
    }, [deletedAgente])


    const theadData = ["Nombre", "Email", "Teléfono", "Área", "Acciones"];

    return (
        <>
            <SideBar />
            <main className="content">
                {/* <Navbar /> */}
                {/* <CustomTable /> */}
                {/* <Example /> */}
                {isLoading ? (
                    <p>loading...</p>
                ) : (
                    <div>
                        {error && (
                            <div>
                                <p>{error.message}</p>
                            </div>
                        )}
                        <div> {
                            // no need to use another state to store data, response is sufficient
                            /* data && <p>{data.id}</p> */
                            /* data && <Table theadData={theadData} tbodyData={data.agentes.map((agente, i) => ({ id: i, items: [agente.nombreCompleto, <i className="fa-solid fa-pen-to-square"></i>] }))} customClass={"table table-centered table-nowrap mb-0 rounded"} customHeadClass={"thead-light"} /> */
                            data ? (
                                <>
                                    <h2 className="mt-2 mb-2">Gestión de agentes</h2>
                                    <hr />
                                    <Table
                                        theadData={theadData}
                                        tbodyData={tBody}
                                        customClass={"table table-centered table-nowrap mb-0 rounded"}
                                        customHeadClass={"thead-light"}
                                    />
                                    <div className="mt-2">
                                        <button onClick={onNewAgente} className="btn btn-gray-800 animate-up-2">Nuevo agente</button>
                                    </div>
                                </>
                            ) : (
                                <></>
                            )
                        }
                        </div>
                    </div>
                )}
            </main>
        </>
    )
}
