import React from 'react'
import { useState } from 'react'
import { useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';

import env from "react-dotenv";
import { useAxios } from '../../hooks';

import { SideBar, Table } from '../../general/components'
import axios from 'axios';
import { callToastWithNavigation } from '../../helpers';


export const AdminList = () => {
    const navigate = useNavigate();
    const the_token = localStorage.getItem('the_token');
    /* const [updatedAdmin, setUpdatedAdmin] = useState(''); */
    const [deletedAdmin, setDeletedAdmin] = useState('')

    const { data, isLoading, error } = useAxios({
        method: 'get',
        url: `${env.BACK_URI}/admins/`,
        headers: {
            'auth-token': the_token
        },
        params: {
            limit: 1000000
        }
    });
    const [newData, setNewData] = useState([])

    const onDeleteAdmin = async (idAdmin) => {
        try {
            const res = await axios.delete(
                `${env.BACK_URI}/admins/${idAdmin}`,
                {
                    headers: { "auth-token": the_token }
                }
            );
            //console.log(res)
            if (res.status == 202) {
                if (res.data?.adminDB?.nombreCompleto) {
                    callToastWithNavigation(`Administrador eliminado correctamente: ${res.data.adminDB.nombreCompleto}`, { type: "success" }, () => { })
                    setDeletedAdmin(res.data.adminDB.nombreCompleto);
                    //console.log(updatedAdmin);
                }
            }
        } catch (error) {
            callToastWithNavigation("Ocurrió un error", { type: "error" }, () => { })
        }
    }

    const onNewAdmin = () => {
        navigate("/admin");
    }

    const [tBody, setTBody] = useState([]);

    const updateBody = (the_data) => {
        if (the_data) {
            const newTBody = the_data.map(
                (admin, i) => (
                    {
                        id: admin.uid,
                        items: [
                            admin.nombreCompleto,
                            admin.email,
                            admin.telefono,
                            <span>
                                <Link to="/admin" state={{ tipo: "editar", id: admin.uid }} className="fa-solid fa-pen-to-square"></Link>
                                &nbsp;&nbsp;&nbsp;
                                <Link onClick={(e) => onDeleteAdmin(admin.uid)} className="fa-solid fa-trash text-danger"></Link>
                            </span>
                        ]
                    }
                )
            );
            setTBody(newTBody);
        }
    }

    useEffect(() => {
        //updateBody(data)
        setNewData(data?.admins)
    }, [data])

    useEffect(() => {
        updateBody(newData)
    }, [newData]);

    useEffect(() => {
        setNewData(
            (newData) => {
                if (newData) {
                    return newData.filter(admin => admin.nombreCompleto !== deletedAdmin)
                }
            }
        )
    }, [deletedAdmin])


    const theadData = ["Nombre", "Email", "Teléfono", "Acciones"];

    return (
        <>
            <SideBar />
            <main className="content">
                {/* <Navbar /> */}
                {/* <CustomTable /> */}
                {/* <Example /> */}
                {isLoading ? (
                    <p>loading...</p>
                ) : (
                    <div>
                        {error && (
                            <div>
                                <p>{error.message}</p>
                            </div>
                        )}
                        <div> {
                            // no need to use another state to store data, response is sufficient
                            /* data && <p>{data.id}</p> */
                            /* data && <Table theadData={theadData} tbodyData={data.admins.map((admin, i) => ({ id: i, items: [admin.nombreCompleto, <i className="fa-solid fa-pen-to-square"></i>] }))} customClass={"table table-centered table-nowrap mb-0 rounded"} customHeadClass={"thead-light"} /> */
                            data ? (
                                <>
                                    <h2 className="mt-2 mb-2">Gestión de administradores</h2>
                                    <hr />
                                    <Table
                                        theadData={theadData}
                                        tbodyData={tBody}
                                        customClass={"table table-centered table-nowrap mb-0 rounded"}
                                        customHeadClass={"thead-light"}
                                    />
                                    <div className="mt-2">
                                        <button onClick={onNewAdmin} className="btn btn-gray-800 animate-up-2">Nuevo administrador</button>
                                    </div>
                                </>
                            ) : (
                                <></>
                            )
                        }
                        </div>
                    </div>
                )}
            </main>
        </>
    )
}
