import React from 'react'
import { useState } from 'react'
import { useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';

import env from "react-dotenv";
import { useAxios } from '../../hooks';

import { SideBar, Table } from '../../general/components'
import axios from 'axios';
import { callToastWithNavigation } from '../../helpers';


export const AreasList = () => {
    const navigate = useNavigate();
    const the_token = localStorage.getItem('the_token');
    /* const [updatedArea, setUpdatedArea] = useState(''); */
    const [deletedArea, setDeletedArea] = useState('')

    const { data, isLoading, error } = useAxios({
        method: 'get',
        url: `${env.BACK_URI}/areas/`,
        headers: {
            'auth-token': the_token
        },
        params: {
            limit: 1000000
        }
    });
    const [newData, setNewData] = useState([])

    const onDeleteArea = async(idArea) => {
        try {
            const res = await axios.delete(
                `${env.BACK_URI}/areas/${idArea}`,
                {
                    headers: { "auth-token": the_token }
                }
            );
            //console.log(res)
            if (res.status == 202) {
                if (res.data?.areaDB?.nombreArea) {
                    callToastWithNavigation(`Área eliminada correctamente: ${res.data.areaDB.nombreArea}`, {type:"success"}, () => {})
                    setDeletedArea(res.data.areaDB.nombreArea);
                    //console.log(updatedArea);
                }
            }
        } catch (error) {
            callToastWithNavigation("Ocurrió un error", {type:"error"}, () => {})
        }
    }
    
    const onNewArea = () => {
        navigate("/area");
    }

    const [tBody, setTBody] = useState([]);

    const updateBody = (the_data) => {
        if(the_data) {
            const newTBody = the_data.map(
                (area, i) => (
                    {   
                        id: area._id, 
                        items: [
                            area.nombreArea,
                            <span>
                            
                                <Link to="/area" state={{tipo:"editar", id:area._id}} className="fa-solid fa-pen-to-square"></Link>
                                
                                &nbsp;&nbsp;&nbsp;
                                <Link onClick={(e) => onDeleteArea(area._id)} className="fa-solid fa-trash text-danger"></Link>
                            </span>
                        ] 
                    }
                )
            );    
            setTBody(newTBody)
        }
    }

    useEffect(() => {
        //updateBody(data)
        setNewData(data?.areas)
    }, [data])

    useEffect(() => {
        updateBody(newData)
    }, [newData]);

    useEffect(() => {
        setNewData(
            (newData) => {
                if(newData) {
                    return newData.filter(area => area.nombreArea !== deletedArea)
                }
            }
        )
    }, [deletedArea])
    

    const theadData = ["Nombre", "Acciones"];

    return (
        <>
            <SideBar />
            <main className="content">
                {/* <Navbar /> */}
                {/* <CustomTable /> */}
                {/* <Example /> */}
                {isLoading ? (
                    <p>loading...</p>
                ) : (
                    <div>
                        {error && (
                            <div>
                                <p>{error.message}</p>
                            </div>
                        )}
                        <div> {
                            // no need to use another state to store data, response is sufficient
                            /* data && <p>{data.id}</p> */
                            /* data && <Table theadData={theadData} tbodyData={data.areas.map((area, i) => ({ id: i, items: [area.nombreArea, <i className="fa-solid fa-pen-to-square"></i>] }))} customClass={"table table-centered table-nowrap mb-0 rounded"} customHeadClass={"thead-light"} /> */
                            data ? (
                                <>
                                    <h2 className="mt-2 mb-2">Gestión de áreas</h2>
                                    <hr />
                                    <Table 
                                        theadData={theadData} 
                                        tbodyData={tBody} 
                                        customClass={"table table-centered table-nowrap mb-0 rounded"} 
                                        customHeadClass={"thead-light"} 
                                    />
                                    <div className="mt-2">
                                        <button onClick={onNewArea} className="btn btn-gray-800 animate-up-2">Nueva área</button>
                                    </div>
                                </>
                            ): (
                                <></>
                            )
                        }
                        </div>
                    </div>
                )}


            </main>
        </>
    )
}
