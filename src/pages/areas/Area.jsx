import React from 'react'
import { useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

import env from "react-dotenv";
import axios from 'axios';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { useForm } from '../../hooks';
import { SideBar } from '../../general/components';
import { callToastWithNavigation } from '../../helpers';

export const Area = () => {
    const location = useLocation();
    let tipo = location.state?.tipo || "crear";
    let id = location.state?.id || "";
    const navigate = useNavigate();

    // Inicializar formulario
    const { setFormState, formState, onInputChange, onResetForm, area } = useForm({
        area: ''
    });

    // obtener token
    const the_token = localStorage.getItem('the_token') || '';
    let theError;
    const onCancel = () => {
        navigate("/areas");
    }

    // Cargar info si es para edición
    useEffect(() => {
        try {
            if (tipo == "editar") {
                const getArea = async (idArea) => {
                    const fullURI = `${env.BACK_URI}/areas/${idArea}`;
                    const resOld = await axios.get(
                        fullURI,
                        {
                            headers: { "auth-token": the_token }
                        }
                    );
                    return resOld;
                }

                getArea(id).then((res) => {
                    if (res.data?.area?.nombreArea) {
                        setFormState({ ...formState, "area": res.data.area.nombreArea })
                    }
                });
            }
        } catch (error) {
            toast.error(error.response);
        }
    }, [tipo])

    // Guardar
    const onSave = async (event) => {
        event.preventDefault();

        // Guardar nuevo registro
        if (tipo == "crear") {
            if (area.trim() !== '') {
                const fullURI = `${env.BACK_URI}/areas`;

                try {
                    //en el endpoint (back) se verifica si existe y el estado es = false, para actualizar en lugar de crear
                    const res = await axios.post(
                        fullURI,
                        { "nombreArea": area },
                        {
                            headers: { "auth-token": the_token }
                        }
                    )

                    // confirmar registro
                    if (res.status == 201 || res.status == 200) {
                        callToastWithNavigation("Área registrada correctamente", { type: "success" }, () => navigate("/areas"));
                    }

                } catch (error) {
                    theError = error.response;
                    if (theError?.data?.errors?.length || 0 > 0) {
                        const concatErrors = theError.data.errors.reduce((prev, curr, idx) => idx == 0 ? curr.msg : prev + "\r\n" + curr.msg, "")
                        return toast.error(concatErrors, { style: { width: "600px" }, position: toast.POSITION.TOP_CENTER })
                    } else {
                        return toast.error(theError);
                    }
                }
            }
        } else {
            // si es tipo edición
            try {
                const fullURI = `${env.BACK_URI}/areas/${id}`;

                // actualizar registro
                console.log(fullURI);
                console.log(area)
                const res = await axios.put(
                    fullURI,
                    {
                        "nombreArea": area
                    },
                    {
                        headers: { "auth-token": the_token }
                    }
                );
                console.log(res)
                // confirmar cambio
                if (res.status == 200) {
                    callToastWithNavigation("Área actualizada correctamente", { type: "success" }, () => navigate("/areas"));
                }

            } catch (error) {
                theError = error.response;
                if (theError?.data.errors?.length || 0 > 0) {
                    const concatErrors = theError.data.errors.reduce((prev, curr, idx) => idx == 0 ? curr.msg : prev + "\r\n" + curr.msg, "")
                    return toast.error(concatErrors, { style: { width: "600px" }, position: toast.POSITION.TOP_CENTER })
                } else {
                    if (theError.data.msg) {
                        return toast.error(theError.data.msg);
                    } else {
                        return toast.error(theError);
                    }
                }
            }
        }
    }



    return (
        <>
            <SideBar />
            <main className="content">
                <div className="row mt-3">
                    <div className="col-12 col-xl-8">
                        <div className="card card-body border-0 shadow mb-4">
                            <h2 className="h5 mb-4">{(tipo == "crear") ? "Registrar nueva área" : "Editar área"}</h2>
                            <form>
                                <div className="row">
                                    <div className="col-md-6 mb-3">
                                        <div>
                                            <label htmlFor="area">Nombre área</label>
                                            <input value={area} onChange={onInputChange} name="area" className="form-control" id="area" type="text" placeholder="Ingresar un nombre" required />
                                        </div>
                                    </div>
                                    {/* <div className="col-md-6 mb-3">
                                        <div>
                                            <label htmlFor="last_name">Last Name</label>
                                            <input className="form-control" id="last_name" type="text" placeholder="Also your last name" required />
                                        </div>
                                    </div> */}
                                </div>
                                <div className="row mt-2">
                                    <div className="col-md-2">
                                        <button onClick={(e) => onSave(e)} className="btn btn-gray-800 mt-2 animate-up-2 w-100" required>Guardar</button>
                                    </div>
                                    <div className="col-md-2">
                                        <button onClick={onCancel} className="btn btn-gray-800 mt-2 animate-up-2 w-100">Cancelar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </main>
        </>
    )
}

