import React, { useState } from 'react'
import { useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

import env from "react-dotenv";
import axios from 'axios';
import dateFormat, { masks } from "dateformat";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { useAxios, useForm } from '../../hooks';
import { SideBar } from '../../general/components';
import { callToastWithNavigation } from '../../helpers';
import DateTimePicker from 'react-datetime-picker';
import { useRef } from 'react';

export const Ticket = () => {
    const location = useLocation();
    let tipo = location.state?.tipo || "crear";
    let id = location.state?.id || "";
    const navigate = useNavigate();

    // obtener token
    const the_token = localStorage.getItem('the_token') || '';

    // Inicializar formulario
    const {
        setFormState, formState, onInputChange, onResetForm,
        cliente, tipoTicket, estadoProceso, agenteAsigna, descripcion, solucion, observacion
    } = useForm({
        cliente: '',
        tipoTicket: '',
        estadoProceso: '',
        agenteAsigna: '',
        descripcion: '',
        solucion: '',
        observacion: ''
    });

    const [fechaApertura, setFechaApertura] = useState(new Date());
    const [fechaCierre, setFechaCierre] = useState();

    // lista de opciones de tipo ticket 
    const [tipoOptions, setTipoOptions] = useState([]);
    const { data: optTipos, isLoading: isLoadingTipos, error: errorTipo } = useAxios({
        method: 'get',
        url: `${env.BACK_URI}/tipos/`,
        headers: {
            'auth-token': the_token
        },
        params: {
            limit: 1000000
        }
    });

    // lista de opciones de estado proceso
    const [estadoOptions, setEstadoOptions] = useState([]);
    const { data: optEstados, isLoading: isLoadingEstados, error: errorEstado } = useAxios({
        method: 'get',
        url: `${env.BACK_URI}/estados/`,
        headers: {
            'auth-token': the_token
        },
        params: {
            limit: 1000000
        }
    });

    // lista de opciones de agentes
    const [agenteOptions, setAgenteOptions] = useState([]);
    const { data: optAgentes, isLoading: isLoadingAgentes, error: errorAgente } = useAxios({
        method: 'get',
        url: `${env.BACK_URI}/agentes/`,
        headers: {
            'auth-token': the_token
        },
        params: {
            limit: 1000000
        }
    });

    useEffect(() => {
        if (!isLoadingTipos && !errorTipo) {
            if (optTipos.tipos) {
                setTipoOptions(
                    optTipos.tipos.map((tipo) => {
                        return {
                            value: tipo.descripcionTipo,
                            label: tipo.descripcionTipo,
                            _id: tipo._id
                        }
                    })
                );
                // actualizar en el state, si es creación
                if (tipo == "crear") {
                    setFormState({ ...formState, tipoTicket: optTipos.tipos[0].descripcionTipo });
                }
            }
        }
    }, [isLoadingTipos])

    useEffect(() => {
        if (!isLoadingEstados && !errorEstado) {
            if (optEstados.estados) {
                setEstadoOptions(
                    optEstados.estados.map((estado) => {
                        return {
                            value: estado.nombreEstado,
                            label: estado.nombreEstado,
                            _id: estado._id
                        }
                    })
                );
                // actualizar en el state, si es creación
                if (tipo == "crear") {
                    /* setFormState({ ...formState, estadoProceso: optEstados.estados[0].nombreEstado }); */
                    setFormState({ ...formState, estadoProceso: 'ABIERTO' });
                }
            }
        }
    }, [isLoadingEstados])

    useEffect(() => {
        if (!isLoadingAgentes && !errorAgente) {
            if (optAgentes.agentes) {
                setAgenteOptions(
                    optAgentes.agentes.map((agente) => {
                        return {
                            value: agente.nombreCompleto,
                            label: agente.nombreCompleto,
                            uid: agente.uid
                        }
                    })
                );
            }
        }
    }, [isLoadingAgentes])

    // para capturar el id del agente
    const selectRef = useRef();
    const [idAgenteSeleccionado, setIdAgenteSeleccionado] = useState('');
    const [idAgenteAnterior, setidAgenteAnterior] = useState('');
    useEffect(() => {
        //document.querySelector("#agenteAsigna")
        //console.log(selectRef)
        if (!isLoadingAgentes && !isLoadingEstados && !isLoadingTipos) {
            if (selectRef.current?.selectedIndex) {
                let index = selectRef.current.selectedIndex;
                let optionElement = selectRef.current.childNodes[index];
                let opt = optionElement.getAttribute('data-key');
                setIdAgenteSeleccionado(opt);
            }
        }
    }, [agenteAsigna, isLoadingAgentes, isLoadingEstados, isLoadingTipos])




    let theError;
    const onCancel = () => {
        navigate("/tickets");
    }

    // Cargar info si es para edición
    let actTicket;
    let lastHist;
    useEffect(() => {
        if (!isLoadingAgentes && !isLoadingEstados && !isLoadingTipos) {
            try {
                if (tipo == "editar") {
                    if (!actTicket) {
                        const getTicket = async (idTicket) => {
                            const fullURI = `${env.BACK_URI}/tickets/${idTicket}`;
                            const resOld = await axios.get(
                                fullURI,
                                {
                                    headers: { "auth-token": the_token }
                                }
                            );
                            return resOld;
                        }

                        getTicket(id).then((res) => {
                            if (res.data.ticket) {
                                actTicket = res.data.ticket;
                                //consultar la última historia
                                const getTicketLastHist = async (idTicket) => {
                                    const fullURI = `${env.BACK_URI}/tickets/${idTicket}/last`;
                                    const resOld = await axios.get(
                                        fullURI,
                                        {
                                            headers: { "auth-token": the_token }
                                        }
                                    );
                                    return resOld;
                                }

                                getTicketLastHist(id).then((res) => {
                                    if (res.data.ticket) {
                                        lastHist = res.data;
                                        //----
                                        const ticket = actTicket;
                                        const hist = lastHist;
                                        setFormState(
                                            {
                                                ...formState,
                                                cliente: ticket.cliente.email,
                                                tipoTicket: ticket.tipoTicket.descripcionTipo,
                                                estadoProceso: ticket.estadoProceso.nombreEstado,
                                                agenteAsigna: hist.agenteAsigna?.nombreCompleto || '',
                                                descripcion: ticket.descripcion,
                                                solucion: ticket.solucion
                                            }
                                        )
                                        setFechaApertura(new Date(ticket.fechaApertura));
                                        if (ticket.fechaCierre) {
                                            setFechaCierre(new Date(ticket.fechaCierre));
                                        }
                                        setIdAgenteSeleccionado(hist.agenteAsigna._id)
                                        setidAgenteAnterior(hist.agenteAsigna._id)
                                        //----
                                    }
                                });
                            }
                        });
                    }

                }
            } catch (error) {
                toast.error(error.response);
            }
        }
    }, [tipo, isLoadingAgentes, isLoadingEstados, isLoadingEstados])

    // Guardar
    const onSave = async (event) => {
        event.preventDefault();

        // Guardar nuevo registro
        if (tipo == "crear") {
            const fullURI = `${env.BACK_URI}/tickets`;
            const fullURIH = `${env.BACK_URI}/historias`;
            try {
                const res = await axios.post(
                    fullURI,
                    {
                        cliente, tipoTicket, estadoProceso, fechaApertura, descripcion, solucion: '', fechaCierre: ''
                    },
                    {
                        headers: { "auth-token": the_token }
                    }
                )

                // guardar la historia
                if (res.status == 201 || res.status == 200) {
                    const ticket = res.data.ticket;
                    const resH = await axios.post(
                        fullURIH,
                        {
                            idTicket: ticket._id,
                            agenteAsigna: idAgenteSeleccionado,
                            evento: 'apertura',
                            observacion
                        },
                        {
                            headers: { "auth-token": the_token }
                        }
                    )

                    // confirmar registro
                    if ((res.status == 201 || res.status == 200) && (resH.status == 200 || resH.status == 201)) {
                        callToastWithNavigation("Ticket registrado correctamente", { type: "success" }, () => navigate("/tickets"));
                    } else {
                        toast.error("Ocurrió un error al registrar el ticket")
                    }
                }


            } catch (error) {
                theError = error.response;
                if (theError?.data?.errors?.length || 0 > 0) {
                    const concatErrors = theError.data.errors.reduce((prev, curr, idx) => idx == 0 ? curr.msg : prev + "\r\n" + curr.msg, "")
                    return toast.error(concatErrors, { style: { width: "600px" }, position: toast.POSITION.TOP_CENTER })
                } else {
                    return toast.error(theError);
                }
            }
        } else {
            // si es tipo edición
            try {
                const fullURI = `${env.BACK_URI}/tickets/${id}`;
                const fullURIH = `${env.BACK_URI}/historias`;

                //cliente, tipoTicket, estadoProceso, fechaApertura, descripcion, solucion: '', fechaCierre: ''

                const newTicket = {
                    descripcion,
                    cliente,
                    estadoProceso,
                    fechaCierre,
                    solucion,
                    tipoTicket
                }

                // actualizar registro
                //console.log(newTicket)
                const res = await axios.put(
                    fullURI,
                    newTicket,
                    {
                        headers: { "auth-token": the_token }
                    }
                );

                // guardar la historia
                if (res.status == 201 || res.status == 200) {
                    //const ticket = res.data.ticket;
                    let evento = 'seguimiento';
                    if (idAgenteAnterior !== idAgenteSeleccionado) {
                        evento = 'reasignacion'
                    }
                    if (estadoProceso == 'CANCELADO' || estadoProceso == 'SOLUCIONADO') {
                        evento = 'cierre'
                    }
                    const resH = await axios.post(
                        fullURIH,
                        {
                            idTicket: id,
                            agenteAsigna: idAgenteSeleccionado,
                            evento,
                            observacion
                        },
                        {
                            headers: { "auth-token": the_token }
                        }
                    )

                    // confirmar registro
                    if ((res.status == 201 || res.status == 200) && (resH.status == 200 || resH.status == 201)) {
                        callToastWithNavigation("Ticket actualizado correctamente", { type: "success" }, () => navigate("/tickets"));
                    } else {
                        toast.error("Ocurrió un error al actualizar el ticket")
                    }
                }

            } catch (error) {
                theError = error.response;
                if (theError?.data.errors?.length || 0 > 0) {
                    const concatErrors = theError.data.errors.reduce((prev, curr, idx) => idx == 0 ? curr.msg : prev + "\r\n" + curr.msg, "")
                    return toast.error(concatErrors, { style: { width: "600px" }, position: toast.POSITION.TOP_CENTER })
                } else {
                    if (theError.data.msg) {
                        return toast.error(theError.data.msg);
                    } else {
                        return toast.error(theError);
                    }
                }
            }
        }
    }

    return (
        <>
            <SideBar />
            <main className="content">
                <div className="row mt-3">
                    <div className="col-12 col-xl-8">
                        <div className="card card-body border-0 shadow mb-4">
                            <h2 className="h5 mb-4">{(tipo == "crear") ? "Registrar nuevo ticket" : "Editar ticket"}</h2>
                            <form>
                                <div className="row">
                                    <div className="col-md-6 mb-3">
                                        <div>
                                            <label htmlFor="nombre">Cliente</label>
                                            <input value={cliente} onChange={onInputChange} name="cliente" className="form-control" id="cliente" type="text" placeholder="ejemplo@dominio.com" disabled={(tipo == 'crear') ? false : true} required />
                                        </div>
                                    </div>
                                    <div className="col-md-6 mb-3">
                                        <div>
                                            <label htmlFor="tipoTicket">Tipo ticket</label>
                                            <select name="tipoTicket" value={tipoTicket} onChange={onInputChange} className="form-select w-100 mb-0" id="tipoTicket" required>
                                                {
                                                    tipoOptions.map((opt) => (
                                                        <option key={opt._id} value={opt.value}>{opt.label}</option>
                                                    ))
                                                }
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-6 mb-3">
                                        <div>
                                            <label htmlFor="estadoProceso">Estado proceso</label>
                                            <select name="estadoProceso" value={estadoProceso} onChange={onInputChange} className="form-select w-100 mb-0" id="estadoProceso" required disabled={(tipo == 'crear') ? true : false}>
                                                {
                                                    estadoOptions.map((opt) => (
                                                        <option key={opt._id} value={opt.value}>{opt.label}</option>
                                                    ))
                                                }
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-md-6 mb-3">
                                        <div>
                                            <label htmlFor="agenteAsigna">Agente asignado</label>
                                            <select ref={selectRef} name="agenteAsigna" value={agenteAsigna} onChange={onInputChange} className="form-select w-100 mb-0" id="agenteAsigna" >
                                                {
                                                    <>
                                                        <option key={'0'} value={''} data-key={''}></option>
                                                        {agenteOptions.map((opt) => (
                                                            <option key={opt.uid} value={opt.value} data-key={opt.uid}>{opt.label}</option>
                                                        ))}
                                                    </>
                                                }
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-6 mb-3">
                                        <div>
                                            <label htmlFor="fechaApertura">Fecha apertura</label>
                                            {/* <input value={fechaApertura} onChange={onInputChange} name="fechaApertura" className="form-control" id="fechaApertura" type="text" placeholder="" required /> */}
                                            <DateTimePicker /* format="y-MM-dd h:mm:ss a" */ value={fechaApertura} onChange={setFechaApertura} name="fechaApertura" className="form-control" id="fechaApertura" />
                                        </div>
                                    </div>
                                    <div className="col-md-6 mb-3">
                                        <div>
                                            <label htmlFor="fechaCierre">Fecha Cierre</label>
                                            {/* <input value={fechaCierre} onChange={onInputChange} name="fechaCierre" className="form-control" id="fechaCierre" type="text" placeholder="" required /> */}
                                            <DateTimePicker /* format="y-MM-dd h:mm:ss a" */ className="form-control" value={fechaCierre} onChange={setFechaCierre} name="fechaCierre" id="fechaCierre" required={(estadoProceso == 'SOLUCIONADO' || estadoProceso == 'CERRADO')} disabled={(tipo == 'crear') ? true : false} />
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-12 mb-3">
                                        <div>
                                            <label htmlFor="descripcion">Descripción</label>
                                            <textarea className="form-control w-100 mb-0" value={descripcion} onChange={onInputChange} name="descripcion" id="descripcion" disabled={(tipo == 'crear') ? false : true} cols="100" rows="10"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-12 mb-3">
                                        <div>
                                            <label htmlFor="descripcion">Añadir observaciones</label>
                                            <textarea className="form-control w-100 mb-0" value={observacion} onChange={onInputChange} name="observacion" id="observacion" cols="100" rows="2"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-12 mb-3">
                                        <div>
                                            <label htmlFor="descripcion">Solución</label>
                                            <textarea className="form-control w-100 mb-0" value={solucion} onChange={onInputChange} name="solucion" id="solucion" required={(estadoProceso == 'SOLUCIONADO')} disabled={(tipo == 'crear') ? true : false} cols="100" rows="2"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div className="row mt-2">
                                    <div className="col-md-2">
                                        <button onClick={(e) => onSave(e)} className="btn btn-gray-800 mt-2 animate-up-2 w-100" required>Guardar</button>
                                    </div>
                                    <div className="col-md-2">
                                        <button onClick={onCancel} className="btn btn-gray-800 mt-2 animate-up-2 w-100">Cancelar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </main>
        </>
    )
}

