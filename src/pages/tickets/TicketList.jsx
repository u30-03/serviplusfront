import React from 'react'
import { useState } from 'react'
import { useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';

import axios from 'axios';
import dateFormat, { masks } from "dateformat";

import env from "react-dotenv";
import { useAxios } from '../../hooks';
import { SideBar, Table } from '../../general/components'
import { callToastWithNavigation } from '../../helpers';


export const TicketList = () => {
    const navigate = useNavigate();
    const the_token = localStorage.getItem('the_token');
    const [deletedTicket, setDeletedTicket] = useState('');

    const { data, isLoading, error } = useAxios({
        method: 'get',
        url: `${env.BACK_URI}/tickets/`,
        headers: {
            'auth-token': the_token
        },
        params: {
            limit: 1000000
        }
    });
    const [newData, setNewData] = useState([])

    const onDeleteTicket = async (idTicket) => {
        try {
            const res = await axios.delete(
                `${env.BACK_URI}/tickets/${idTicket}`,
                {
                    headers: { "auth-token": the_token }
                }
            );
            //console.log(res)
            if (res.status == 202) {
                if (res.data?.ticketDB) {
                    callToastWithNavigation(`Ticket eliminado correctamente`, { type: "success" }, () => { })
                    //console.log(res.data.ticketDB._id);
                    setDeletedTicket(res.data.ticketDB._id);
                    //console.log(updatedTicket);
                }
            }
        } catch (error) {
            callToastWithNavigation("Ocurrió un error", { type: "error" }, () => { })
        }
    }

    const onNewTicket = () => {
        navigate("/ticket");
    }

    const [tBody, setTBody] = useState([]);

    const updateBody = (the_data) => {
        if (the_data) {
            const newTBody = the_data.map(
                (ticket, i) => (
                    {
                        id: ticket._id,
                        items: [
                            ticket.cliente.nombreCompleto,
                            ticket.cliente.email,
                            ticket.tipoTicket.descripcionTipo,
                            ticket.estadoProceso.nombreEstado,
                            /* ticket.fechaApertura, */
                            dateFormat(ticket.fechaApertura, 'dd-mmm-yyyy HH:MM'),
                            /* ticket.fechaCierre, */
                            dateFormat(ticket.fechaCierre, 'dd-mmm-yyyy HH:MM') || '',
                            <span>
                                <Link to="/ticket" state={{ tipo: "editar", id: ticket._id }} className="fa-solid fa-pen-to-square"></Link>
                                &nbsp;&nbsp;&nbsp;
                                <Link onClick={(e) => onDeleteTicket(ticket._id)} className="fa-solid fa-trash text-danger"></Link>
                            </span>
                        ]
                    }
                )
            );
            setTBody(newTBody);
        }
    }

    useEffect(() => {
        //updateBody(data)
        setNewData(data?.tickets)
    }, [data])

    useEffect(() => {
        updateBody(newData)
    }, [newData]);

    useEffect(() => {
        setNewData(
            (newData) => {
                if (newData) {
                    return newData.filter(ticket => ticket._id !== deletedTicket)
                }
            }
        )
    }, [deletedTicket])


    const theadData = ["Cliente", "Email", "Tipo ticket", "Estado", "Fecha Apertura", "Fecha Cierre", "Acciones"];

    return (
        <>
            <SideBar />
            <main className="content">
                {/* <Navbar /> */}
                {/* <CustomTable /> */}
                {/* <Example /> */}
                {isLoading ? (
                    <p>loading...</p>
                ) : (
                    <div>
                        {error && (
                            <div>
                                <p>{error.message}</p>
                            </div>
                        )}
                        <div> {
                            data ? (
                                <>
                                    <h2 className="mt-2 mb-2">Gestión de tickets</h2>
                                    <hr />
                                    <Table
                                        theadData={theadData}
                                        tbodyData={tBody}
                                        customClass={"table table-centered table-nowrap mb-0 rounded"}
                                        customHeadClass={"thead-light"}
                                    />
                                    <div className="mt-2">
                                        <button onClick={onNewTicket} className="btn btn-gray-800 animate-up-2">Nuevo ticket</button>
                                    </div>
                                </>
                            ) : (
                                <></>
                            )
                        }
                        </div>
                    </div>
                )}
            </main>
        </>
    )
}
