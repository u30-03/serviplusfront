/* import { useContext } from 'react'; */
import { useNavigate } from 'react-router-dom';
import env from "react-dotenv";
import { toast } from 'react-toastify';
import axios from 'axios';
import { useForm } from '../../hooks';
import { useState } from 'react';
/* import { AuthContext } from '../context/AuthContext'; */

export const LoginPage = () => {
    //acá hacer un useform y un useAxios
    const { formState, onInputChange, onResetForm, email, password } = useForm({
        email: 'admin@serviplus.com',
        password: '123456aB'
    });

    const [loginType, setLoginType] = useState("agente");

    const onChangeType = (event) => {
        setLoginType(event.target.value);
    }


    /* const { login } = useContext( AuthContext ); */
    const navigate = useNavigate();

    /* const { data, isLoading, error } = useAxios({
        method: 'post',
        url: `${env.BACK_URI}/auth/admin`,
        body: JSON.stringify({
            email: 'admin@serviplus.com',
            password: '123456aB'
        })
    })
    console.log(data, isLoading, error); */

    const onLogin = async (event, logType) => {
        event.preventDefault();
        const lastPath = localStorage.getItem('lastPath') || '/';

        const fullURI = `${env.BACK_URI}/auth/${logType}`

        try {

            const res = await axios.post(
                fullURI,
                {
                    email,
                    password
                }
            )
            if (res.data.token) {
                /* console.log(res); */
                /* localStorage.setItem('uid', res.data.user.uid); */
                localStorage.setItem('the_token', res.data.token);

                navigate(lastPath, {
                    replace: true
                });
            }

        } catch (error) {
            const theError = error.response;
            if (theError?.data.errors?.length || 0 > 0) {
                const concatErrors = theError.data.errors.reduce((prev, curr, idx) => idx == 0 ? curr.msg : prev + "\r\n" + curr.msg, "")
                return toast.error(concatErrors, { style: { width: "600px" }, position: toast.POSITION.TOP_CENTER })
            } else {
                if (theError.data.msg) {
                    return toast.error(theError.data.msg);
                } else {
                    return toast.error(theError);
                }
            }
        }
    }

    return (
        <>
            {/* <head>
                <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
                <title>ServiPlus</title>
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
            </head> */}

            <main>

                {/* Section */}
                <section className="vh-lg-100 mt-5 mt-lg-0 bg-soft d-flex align-items-center">
                    <div className="container">
                        <p className="text-center">
                            <a href="/home" className="d-flex align-items-center justify-content-center">
                                <svg className="icon icon-xs me-2" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M7.707 14.707a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 1.414L5.414 9H17a1 1 0 110 2H5.414l2.293 2.293a1 1 0 010 1.414z" clipRule="evenodd"></path></svg>
                                Volver al inicio
                            </a>
                        </p>
                        <div className="row justify-content-center form-bg-image" data-background-lg="../../assets/img/illustrations/signin.svg">
                            <div className="col-12 d-flex align-items-center justify-content-center">
                                <div className="bg-white shadow border-0 rounded border-light p-4 p-lg-5 w-100 fmxw-500">
                                    <div className="text-center text-md-center mb-4 mt-md-0">
                                        <h1 className="mb-0 h3">Ingreso de usuarios</h1>
                                    </div>
                                    <form action="#" className="mt-4" id="formLogin">
                                        {/* Form */}
                                        <div className="form-group mb-4">
                                            <label htmlFor="email">Email</label>
                                            <div className="input-group">
                                                <span className="input-group-text" id="basic-addon1">
                                                    <svg className="icon icon-xs text-gray-600" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path><path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path></svg>
                                                </span>
                                                <input type="email" name="email" value={email} onChange={onInputChange} className="form-control" placeholder="ejemplo@serviplus.com" id="email" autoFocus required />
                                            </div>
                                        </div>
                                        {/* End of Form  */}
                                        <div className="form-group">
                                            {/* Form */}
                                            <div className="form-group mb-4">
                                                <label htmlFor="password">Password</label>
                                                <div className="input-group">
                                                    <span className="input-group-text" id="basic-addon2">
                                                        <svg className="icon icon-xs text-gray-600" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z" clipRule="evenodd"></path></svg>
                                                    </span>
                                                    <input type="password" name="password" value={password} onChange={onInputChange} placeholder="Contraseña" className="form-control" id="password" required />
                                                </div>
                                            </div>
                                            <div onChange={onChangeType}>
                                                <label className="form-check-label" htmlFor="tipoIngreso">
                                                    Ingresar como
                                                </label>
                                                <div className="form-check" id="tipoIngreso">
                                                    <input className="form-check-input" value="agente" type="radio" name="radioTipo" defaultChecked={true} />
                                                    <label className="form-check-label" htmlFor="radioAgente">
                                                        Agente
                                                    </label>
                                                </div>
                                                <div className="form-check">
                                                    <input className="form-check-input" value="admin" type="radio" name="radioTipo" />
                                                    <label className="form-check-label" htmlFor="radioAdmin">
                                                        Administrador
                                                    </label>
                                                </div>
                                            </div>
                                            {/* <div className="d-flex justify-content-between align-items-top mb-4">
                                                </div> */}
                                            {/* End of Form */}
                                            <div className="d-flex justify-content-between align-items-top mb-4">
                                                {/* <div className="form-check">
                                                        <input className="form-check-input" type="checkbox" value="" id="remember" />
                                                        <label className="form-check-label mb-0" htmlFor="remember">
                                                            Remember me
                                                        </label>
                                                    </div> */}
                                                {/* <div><a href="/loginadmin" className="small text-right">Ingresar como admin</a></div> */}
                                            </div>
                                        </div>
                                        <div className="d-grid">
                                            <button type="submit" className="btn btn-gray-800" onClick={(e) => onLogin(e, loginType)}>Ingresar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>

            {/* <div className="container mt-5">
            <h1>Login</h1>
            <hr />

            <button
                className="btn btn-primary"
                onClick={onLogin}
            >
                Login
            </button>

        </div> */}
        </>
    )
}