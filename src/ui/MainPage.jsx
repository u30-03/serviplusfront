/* import fontawesome from '@fortawesome/fontawesome' */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleUser, faCirclePlus } from '@fortawesome/free-solid-svg-icons'
import { Team } from './Team'

//fontawesome.library.add(faCheckSquare, faCoffee, faCircleUser, faCirclePlus);

export const MainPage = () => {
    return (
        <>
            {/* <head>
                <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
                <title>ServiPlus</title>
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css"
                    integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A=="
                    crossorigin="anonymous" referrerpolicy="no-referrer" />
            </head> */}


            <main>
                {/* pt-7 pt-lg-8 pb-9 pb-lg-12  */}
                <section className="section-header overflow-hidden bg-primary text-white">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 text-center">
                                <h1 className="fw-bolder">ServiPlus</h1>
                                <h2 className="lead fw-normal text-muted mb-5">Tickets de servicio</h2>
                                <div className="d-flex align-items-center justify-content-center mb-5">
                                    {/* <a href="./pages/dashboard/dashboard.html"
                                        className="btn btn-secondary d-inline-flex align-items-center me-4">                                        
                                        <span className="me-2"><FontAwesomeIcon icon={faCirclePlus} /></span>
                                        Crear nuevo ticket
                                    </a> */}

                                    <a href="/login"
                                        className="btn btn-secondary d-inline-flex align-items-center">
                                        {/* <span className="me-2"><i className="fa-solid fa-circle-user"></i></span> */}
                                        <span className="me-2"><FontAwesomeIcon icon={faCircleUser} /></span>
                                        Acceso a usuarios
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>
                <section className="container mt-3">
                    <h1 className="fw-bolder mb-5 mt-5 text-center">Desarrollado por</h1>
                    <Team />
                </section>
            </main>

        </>
    )
}
