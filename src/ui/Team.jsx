import React from 'react';
import {
    MDBCard,
    MDBCardImage,
    MDBCardBody,
    MDBCardTitle,
    MDBCardText,
    MDBRow,
    MDBCol
} from 'mdb-react-ui-kit';


export const Team = () => {
    return (
        <>
            <MDBRow className='row-cols-1 row-cols-md-5 g-4'>
                <MDBCol>
                    <MDBCard className='h-100'>
                        {/* <MDBCardImage
                            src='https://res.cloudinary.com/rendonmg/image/upload/v1668465530/mintic/carlos_pkf71q.png'
                            alt='...'
                            position='top'
                        /> */}
                        <MDBCardBody className='d-flex align-items-center'>
                            <MDBCardTitle className='text-center mb-0'>Luis Carlos García Mass</MDBCardTitle>
                            <MDBCardText>
                            </MDBCardText>
                        </MDBCardBody>
                    </MDBCard>
                </MDBCol>
                <MDBCol>
                    <MDBCard className='h-100'>
                        {/* <MDBCardImage
                            src='https://res.cloudinary.com/rendonmg/image/upload/v1668465525/mintic/juan_qy42op.png'
                            alt='...'
                            position='top'
                        /> */}
                        <MDBCardBody className='d-flex align-items-center'>
                            <MDBCardTitle className='text-center mb-0'>Juan Esteban Díaz López</MDBCardTitle>
                            <MDBCardText>
                            </MDBCardText>
                        </MDBCardBody>
                    </MDBCard>
                </MDBCol>
                <MDBCol>
                    <MDBCard className='h-100'>
                        {/* <MDBCardImage                            
                            alt='...'
                            position='top'
                        /> */}
                        <MDBCardBody className='d-flex align-items-center'>
                            <MDBCardTitle className='text-center mb-0'>William David Suárez Arévalo</MDBCardTitle>
                            <MDBCardText>
                            </MDBCardText>
                        </MDBCardBody>
                    </MDBCard>
                </MDBCol>
                <MDBCol>
                    <MDBCard className='h-100'>
                        {/* <MDBCardImage
                            alt='...'
                            position='top'
                        /> */}
                        <MDBCardBody className='d-flex align-items-center'>
                            <MDBCardTitle className='text-center mb-0'>Leonardo Velásquez Anzola</MDBCardTitle>
                            <MDBCardText>
                            </MDBCardText>
                        </MDBCardBody>
                    </MDBCard>
                </MDBCol>
                <MDBCol>
                    <MDBCard className='h-100'>
                        {/* <MDBCardImage
                            src='https://res.cloudinary.com/rendonmg/image/upload/v1668465526/mintic/miguel_b2asrd.jpg'
                            alt='...'
                            position='top'
                        /> */}
                        <MDBCardBody className='d-flex align-items-center'>
                            <MDBCardTitle className='text-center mb-0'>Miguel Ángel Rendón García</MDBCardTitle>
                            <MDBCardText>
                            </MDBCardText>
                        </MDBCardBody>
                    </MDBCard>
                </MDBCol>
            </MDBRow>
        </>
    );
}